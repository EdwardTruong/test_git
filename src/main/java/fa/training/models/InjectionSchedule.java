package fa.training.models;

import jakarta.persistence.*;
import lombok.*;

import java.sql.Date;
import java.util.Objects;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "INJECTION_SCHEDULE", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class InjectionSchedule {
    @Id
    @Basic
    @Column(name = "INJECTION_SCHEDULE_ID", nullable = true, length = 36)
    private String injectionScheduleId;
    @Basic
    @Column(name = "DESCRIPTION", nullable = true, length = 1000)
    private String description;
    @Basic
    @Column(name = "END_DATE", nullable = true)
    private Date endDate;
    @Basic
    @Column(name = "PLACE", nullable = true, length = 255)
    private String place;
    @Basic
    @Column(name = "START_DATE", nullable = true)
    private Date startDate;
    @Basic
    @Column(name = "VACCINE_ID", nullable = true, length = 36)
    private String vaccineId;
    @ManyToOne
    @JoinColumn(name = "VACCINE_ID", referencedColumnName = "VACCINE_ID")
    private Vaccine vaccineByVaccineId;
}
