package fa.training.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "NEWS_TYPE", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class NewsType {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "NEWS_TYPE_ID", nullable = false, length = 36)
    private String newsTypeId;
    @Basic
    @Column(name = "DESCRIPTION", nullable = true, length = 10)
    private String description;
    @Basic
    @Column(name = "NEWS_TYPE_NAME", nullable = true, length = 50)
    private String newsTypeName;
    @OneToMany(mappedBy = "newsTypeByNewsTypeId")
    private Collection<News> newsByNewsTypeId;
}
