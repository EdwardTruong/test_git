package fa.training.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class News {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "NEWS_ID", nullable = false, length = 36)
    private String newsId;
    @Basic
    @Column(name = "CONTENT", nullable = true, length = 4000)
    private String content;
    @Basic
    @Column(name = "PREVIEW", nullable = true, length = 1000)
    private String preview;
    @Basic
    @Column(name = "TITLE", nullable = true, length = 1000)
    private String title;
    @Basic
    @Column(name = "NEWS_TYPE_ID", nullable = true, length = 36)
    private String newsTypeId;
    @ManyToOne
    @JoinColumn(name = "NEWS_TYPE_ID", referencedColumnName = "NEWS_TYPE_ID")
    private NewsType newsTypeByNewsTypeId;
}
