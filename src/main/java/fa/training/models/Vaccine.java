package fa.training.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Vaccine {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "VACCINE_ID", nullable = false, length = 36)
    private String vaccineId;
    @Basic
    @Column(name = "CONTRAINDICATION", nullable = true, length = 200)
    private String contraindication;
    @Basic
    @Column(name = "INDICATION", nullable = true, length = 200)
    private String indication;
    @Basic
    @Column(name = "NUMBER_OF_INJECTION", nullable = true)
    private Integer numberOfInjection;
    @Basic
    @Column(name = "ORIGIN", nullable = true, length = 50)
    private String origin;
    @Basic
    @Column(name = "TIME_BEGIN_NEXT_INJECTION", nullable = true)
    private Date timeBeginNextInjection;
    @Basic
    @Column(name = "TIME_END_NEXT_INJECTION", nullable = true)
    private Date timeEndNextInjection;
    @Basic
    @Column(name = "USEAGE", nullable = true, length = 200)
    private String useage;
    @Basic
    @Column(name = "VACCINE_NAME", nullable = true, length = 100)
    private String vaccineName;
    @Basic
    @Column(name = "VACCIN_TYPE_ID", nullable = true, length = 36)
    private String vaccinTypeId;
    @OneToMany(mappedBy = "vaccineByVaccineId")
    private Collection<InjectionResult> injectionResultsByVaccineId;
    @OneToMany(mappedBy = "vaccineByVaccineId")
    private Collection<InjectionSchedule> injectionSchedulesByVaccineId;
    @ManyToOne
    @JoinColumn(name = "VACCIN_TYPE_ID", referencedColumnName = "VACCINE_TYPE_ID")
    private VaccineType vaccineTypeByVaccinTypeId;
}
