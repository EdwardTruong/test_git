package fa.training.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "INJECTION_RESULT", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class InjectionResult {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "INJECTION_RESULT_ID", nullable = false, length = 36)
    private String injectionResultId;
    @Basic
    @Column(name = "CUSTOMER_ID", nullable = true, length = 36)
    private String customerId;
    @Basic
    @Column(name = "INJECTION_DATE", nullable = true)
    private Date injectionDate;
    @Basic
    @Column(name = "INJECTION_PLACE", nullable = true, length = 255)
    private String injectionPlace;
    @Basic
    @Column(name = "NEXT_INJECTION_DATE", nullable = true)
    private Date nextInjectionDate;
    @Basic
    @Column(name = "NUMBER_OF_INJECTION", nullable = true, length = 100)
    private String numberOfInjection;
    @Basic
    @Column(name = "PREVENTION", nullable = true, length = 100)
    private String prevention;
    @Basic
    @Column(name = "VACCINE_ID", nullable = true, length = 36)
    private String vaccineId;
    @ManyToOne
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
    private Customer customerByCustomerId;
    @ManyToOne
    @JoinColumn(name = "VACCINE_ID", referencedColumnName = "VACCINE_ID")
    private Vaccine vaccineByVaccineId;

}
