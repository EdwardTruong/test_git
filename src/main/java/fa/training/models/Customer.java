package fa.training.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Customer {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "CUSTOMER_ID", nullable = false, length = 36)
    private String customerId;
    @Basic
    @Column(name = "ADDRESS", nullable = true, length = 255)
    private String address;
    @Basic
    @Column(name = "DATE_OF_BIRTH", nullable = true)
    private Date dateOfBirth;
    @Basic
    @Column(name = "EMAIL", nullable = true, length = 100)
    private String email;
    @Basic
    @Column(name = "FULL_NAME", nullable = true, length = 100)
    private String fullName;
    @Basic
    @Column(name = "GENDER", nullable = true)
    private Integer gender;
    @Basic
    @Column(name = "IDENTITY_CARD", nullable = true, length = 12)
    private String identityCard;
    @Basic
    @Column(name = "PASSWORD", nullable = true, length = 255)
    private String password;
    @Basic
    @Column(name = "PHONE", nullable = true, length = 20)
    private String phone;
    @Basic
    @Column(name = "USERNAME", nullable = true, length = 255)
    private String username;
    @OneToMany(mappedBy = "customerByCustomerId")
    private Collection<InjectionResult> injectionResultsByCustomerId;
}
