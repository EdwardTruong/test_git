package fa.training.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.*;

import java.sql.Date;
import java.util.Objects;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "EMPLOYEE_ID", nullable = false, length = 36)
    private String employeeId;
    @Basic
    @Column(name = "ADDRESS", nullable = true, length = 255)
    private String address;
    @Basic
    @Column(name = "DATE_OF_BIRTH", nullable = true)
    private Date dateOfBirth;
    @Basic
    @Column(name = "EMAIL", nullable = true, length = 100)
    private String email;
    @Basic
    @Column(name = "EMPLOYEE_NAME", nullable = true, length = 100)
    private String employeeName;
    @Basic
    @Column(name = "GENDER", nullable = true)
    private Integer gender;
    @Basic
    @Column(name = "IMAGE", nullable = true, length = 255)
    private String image;
    @Basic
    @Column(name = "PASSWORD", nullable = true, length = 255)
    private String password;
    @Basic
    @Column(name = "PHONE", nullable = true, length = 20)
    private String phone;
    @Basic
    @Column(name = "POSITION", nullable = true, length = 100)
    private String position;
    @Basic
    @Column(name = "USERNAME", nullable = true, length = 255)
    private String username;
    @Basic
    @Column(name = "WORKING_PLACE", nullable = true, length = 255)
    private String workingPlace;
}
