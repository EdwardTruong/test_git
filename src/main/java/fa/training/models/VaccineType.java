package fa.training.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "VACCINE_TYPE", schema = "VACCINE", catalog = "VACCINE_MANAGEMENT")
public class VaccineType {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "VACCINE_TYPE_ID", nullable = false, length = 36)
    private String vaccineTypeId;
    @Basic
    @Column(name = "DESCRIPTION", nullable = true, length = 200)
    private String description;
    @Basic
    @Column(name = "VACCINE_TYPE_NAME", nullable = true, length = 50)
    private String vaccineTypeName;
    @OneToMany(mappedBy = "vaccineTypeByVaccinTypeId")
    private Collection<Vaccine> vaccinesByVaccineTypeId;
}
